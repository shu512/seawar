#pragma once
#include "Ship3.h"

class Ship4 : public Ship3
{
public:
	void show();
	Ship4();
	Ship4(int x, int y, bool vert);
	void place_ship(int x, int y, bool vert);
	~Ship4();
};

