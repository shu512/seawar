#pragma once
#include "Ship2.h"
class Ship3 : public Ship2 {
public:
	Ship3();
	Ship3(int x, int y, bool vert);
	void show();
	void place_ship(int x, int y, bool vert);
};