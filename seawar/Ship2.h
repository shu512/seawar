#pragma once
#include "Ship1.h"
class Ship2 : public Ship1
{
public:
	Ship2();
	void show();
	Ship2(int x, int y, bool vert);
	void place_ship(int x, int y, bool vert);
	~Ship2();
};

