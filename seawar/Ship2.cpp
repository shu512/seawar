#include "Ship2.h"
#include <iostream>
using namespace std;
void Ship2::place_ship(int x, int y, bool vert)
{
	this->x = x;
	this->y = y;
	vertical = vert;
}

void  Ship2::show() {
	cout << "Ship2 was located on x: " << x << " and y: " << y << endl;
};

Ship2::Ship2()
{
	place_ship(99, 99, false);
	deck = 2;
}

Ship2::Ship2(int x, int y, bool vert)
{
	place_ship(x, y, vert);
	deck = 2;
	cout << "Ship2 initialization with arguments\n";
}


Ship2::~Ship2()
{
}
