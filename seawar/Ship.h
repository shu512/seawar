#pragma once
#include <iostream>
using namespace std;

class Ship 
{
protected:
	int x, y, deck;
	bool vertical;
public:
	virtual void place_ship(int x, int y, bool vert) = 0;
	Ship() {};
	virtual void show() {
		cout << "Ship0 not located!\n";
	};
};