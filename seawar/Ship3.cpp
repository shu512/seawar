#include "Ship3.h"
#include <iostream>
using namespace std;
void Ship3::place_ship(int x, int y, bool vert)
{
	this->x = x;
	this->y = y;
	vertical = vert;
}

void Ship3::show() {
	cout << "Ship3 was located on x: " << x << " and y: " << y << endl;
};

Ship3::Ship3()
{
	place_ship(99, 99, false);
	deck = 3;
}

Ship3::Ship3(int x, int y, bool vert)
{
	place_ship(x, y, vert);
	deck = 3;
	cout << "Ship3 initialization with arguments\n";
}

