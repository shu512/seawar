#include <glut.h>
#include <cstdio>
#include <time.h> 
#include <iostream>
#include <Windows.h>
#include "Ship4.h"
using namespace std;

int CURRENT_XI = 99, CURRENT_YJ = 99;
int ships_enemy[10][10];
bool VERTICAL = false;
bool movepl = true;
Ship4 sh4;
Ship3 sh3[2];
Ship2 sh2[3];
Ship1 sh1[4] = { Ship1(), Ship1(), Ship1(), Ship1() };
int step = 4;	//-2-����� ����
				//-1-����
				//0-����������� 1-�����
				//1-����������� 2-�����
				//2-����������� 3-�����
				//3-����������� 4-�����


int ships[10][10];// 0 - ������
				  // 1 - ������ �������
				  // 2 - �����
				  // 3 - ����



void floodFillQ(int x, int y, char color)
{
	int r, g, b;
	if (color == 'r'){
		r = 1;
		g = 0;
		b = 0;
	}else if (color == 'g'){
		r = 0;
		g = 1;
		b = 0;
	}else if (color == 'b'){
		r = 0;
		g = 0;
		b = 1;
	}	else if (color == 'w'){
		r = 1;
		g = 1;
		b = 1;
	}
	else if (color == 'q') {
		r = 0;
		g = 0;
		b = 0;
	}
	glColor3d(r, g, b);
	glBegin(GL_QUADS);
	glVertex2d(x, y + 1);
	glVertex2d(x + 39, y + 1);
	glVertex2d(x + 39, y + 40);
	glVertex2d(x, y + 40);
	glEnd();
	glFlush();
}



void Reshape(int width, int height)
{
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D( 0, 1000, 600, 0);
	glMatrixMode(GL_MODELVIEW);
}

void Draw(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(0.0f, 0.0f, 0.0f);
	glLineWidth(1);

	glBegin(GL_LINES);
	int i;
	for (i = 0; i < 11; i++) //draw field
	{
		glVertex2d(20 + i * 40, 20);
		glVertex2d(20 + i * 40, 420);
		glVertex2d(20 , 20 + i * 40);
		glVertex2d(420, 20 + i * 40);

		glVertex2d(580 + i * 40, 20);
		glVertex2d(580 + i * 40, 420);
		glVertex2d(580, 20 + i * 40);
		glVertex2d(980, 20 + i * 40);
	}
	glEnd();

	glFlush();
}

void watchdog(int PAST_X, int  PAST_Y, bool VERTICAL)
{//current = past
	int CURRENT_XI = (PAST_X - 20) / 40;
	int CURRENT_YJ = (PAST_Y - 20) / 40;
	if (step >= 0) {
		if ((PAST_X >= 20) && (PAST_Y >= 20) && (PAST_X < 420) && (PAST_Y < 420)) {
			if (step == 0) {
				if (ships[CURRENT_XI][CURRENT_YJ] == 0)
					floodFillQ(PAST_X, PAST_Y, 'w');
				if (ships[CURRENT_XI][CURRENT_YJ] == 1)
					floodFillQ(PAST_X, PAST_Y, 'g');
				if (ships[CURRENT_XI][CURRENT_YJ] == 2)
					floodFillQ(PAST_X, PAST_Y, 'r');
				if (ships[CURRENT_XI][CURRENT_YJ] == 3)
					floodFillQ(PAST_X, PAST_Y, 'b');
			}
			else if (step == 1) {
				if (ships[CURRENT_XI][CURRENT_YJ] == 0)
					floodFillQ(PAST_X, PAST_Y, 'w');
				if (ships[CURRENT_XI][CURRENT_YJ] == 1)
					floodFillQ(PAST_X, PAST_Y, 'g');
				if (ships[CURRENT_XI][CURRENT_YJ] == 2)
					floodFillQ(PAST_X, PAST_Y, 'r');
				if (ships[CURRENT_XI][CURRENT_YJ] == 3)
					floodFillQ(PAST_X, PAST_Y, 'b');
				if (VERTICAL == false) {
					if (ships[CURRENT_XI + 1][CURRENT_YJ] == 0)
						floodFillQ(PAST_X + 40, PAST_Y, 'w');
					if (ships[CURRENT_XI + 1][CURRENT_YJ] == 1)
						floodFillQ(PAST_X + 40, PAST_Y, 'g');
					if (ships[CURRENT_XI + 1][CURRENT_YJ] == 2)
						floodFillQ(PAST_X + 40, PAST_Y, 'r');
					if (ships[CURRENT_XI + 1][CURRENT_YJ] == 3)
						floodFillQ(PAST_X + 40, PAST_Y, 'b');
				}
				else {
					if (ships[CURRENT_XI][CURRENT_YJ + 1] == 0)
						floodFillQ(PAST_X, PAST_Y + 40, 'w');
					if (ships[CURRENT_XI][CURRENT_YJ + 1] == 1)
						floodFillQ(PAST_X, PAST_Y + 40, 'g');
					if (ships[CURRENT_XI][CURRENT_YJ + 1] == 2)
						floodFillQ(PAST_X, PAST_Y + 40, 'r');
					if (ships[CURRENT_XI][CURRENT_YJ + 1] == 3)
						floodFillQ(PAST_X, PAST_Y, 'b');
				}
			}
			else if (step == 2) {
				if (ships[CURRENT_XI][CURRENT_YJ] == 0)
					floodFillQ(PAST_X, PAST_Y, 'w');
				if (ships[CURRENT_XI][CURRENT_YJ] == 1)
					floodFillQ(PAST_X, PAST_Y, 'g');
				if (ships[CURRENT_XI][CURRENT_YJ] == 2)
					floodFillQ(PAST_X, PAST_Y, 'r');
				if (ships[CURRENT_XI][CURRENT_YJ] == 3)
					floodFillQ(PAST_X, PAST_Y, 'b');
				if (VERTICAL == false) {
					if (ships[CURRENT_XI + 1][CURRENT_YJ] == 0)
						floodFillQ(PAST_X + 40, PAST_Y, 'w');
					if (ships[CURRENT_XI + 1][CURRENT_YJ] == 1)
						floodFillQ(PAST_X + 40, PAST_Y, 'g');
					if (ships[CURRENT_XI + 1][CURRENT_YJ] == 2)
						floodFillQ(PAST_X + 40, PAST_Y, 'r');
					if (ships[CURRENT_XI + 1][CURRENT_YJ] == 3)
						floodFillQ(PAST_X + 40, PAST_Y, 'b');
					if (ships[CURRENT_XI + 2][CURRENT_YJ] == 0)
						floodFillQ(PAST_X + 80, PAST_Y, 'w');
					if (ships[CURRENT_XI + 2][CURRENT_YJ] == 1)
						floodFillQ(PAST_X + 80, PAST_Y, 'g');
					if (ships[CURRENT_XI + 2][CURRENT_YJ] == 2)
						floodFillQ(PAST_X + 80, PAST_Y, 'r');
					if (ships[CURRENT_XI + 2][CURRENT_YJ] == 3)
						floodFillQ(PAST_X + 80, PAST_Y, 'b');
				}
				else {
					if (ships[CURRENT_XI][CURRENT_YJ + 1] == 0)
						floodFillQ(PAST_X, PAST_Y + 40, 'w');
					if (ships[CURRENT_XI][CURRENT_YJ + 1] == 1)
						floodFillQ(PAST_X, PAST_Y + 40, 'g');
					if (ships[CURRENT_XI][CURRENT_YJ + 1] == 2)
						floodFillQ(PAST_X, PAST_Y + 40, 'r');
					if (ships[CURRENT_XI][CURRENT_YJ + 1] == 3)
						floodFillQ(PAST_X, PAST_Y + 40, 'b');
					if (ships[CURRENT_XI][CURRENT_YJ + 2] == 0)
						floodFillQ(PAST_X, PAST_Y + 80, 'w');
					if (ships[CURRENT_XI][CURRENT_YJ + 2] == 1)
						floodFillQ(PAST_X, PAST_Y + 80, 'g');
					if (ships[CURRENT_XI][CURRENT_YJ + 2] == 2)
						floodFillQ(PAST_X, PAST_Y + 80, 'r');
					if (ships[CURRENT_XI][CURRENT_YJ + 2] == 3)
						floodFillQ(PAST_X, PAST_Y + 80, 'b');
				}
			}
			else if (step == 3) {
				if (ships[CURRENT_XI][CURRENT_YJ] == 0)
					floodFillQ(PAST_X, PAST_Y, 'w');
				if (ships[CURRENT_XI][CURRENT_YJ] == 1)
					floodFillQ(PAST_X, PAST_Y, 'g');
				if (ships[CURRENT_XI][CURRENT_YJ] == 2)
					floodFillQ(PAST_X, PAST_Y, 'r');
				if (ships[CURRENT_XI][CURRENT_YJ] == 3)
					floodFillQ(PAST_X, PAST_Y, 'b');
				if (VERTICAL == false) {
					if (ships[CURRENT_XI + 1][CURRENT_YJ] == 0)
						floodFillQ(PAST_X + 40, PAST_Y, 'w');
					if (ships[CURRENT_XI + 1][CURRENT_YJ] == 1)
						floodFillQ(PAST_X + 40, PAST_Y, 'g');
					if (ships[CURRENT_XI + 1][CURRENT_YJ] == 2)
						floodFillQ(PAST_X + 40, PAST_Y, 'r');
					if (ships[CURRENT_XI + 1][CURRENT_YJ] == 3)
						floodFillQ(PAST_X + 40, PAST_Y, 'b');
					if (ships[CURRENT_XI + 2][CURRENT_YJ] == 0)
						floodFillQ(PAST_X + 80, PAST_Y, 'w');
					if (ships[CURRENT_XI + 2][CURRENT_YJ] == 1)
						floodFillQ(PAST_X + 80, PAST_Y, 'g');
					if (ships[CURRENT_XI + 2][CURRENT_YJ] == 2)
						floodFillQ(PAST_X + 80, PAST_Y, 'r');
					if (ships[CURRENT_XI + 2][CURRENT_YJ] == 3)
						floodFillQ(PAST_X + 80, PAST_Y, 'b');
					if (ships[CURRENT_XI + 3][CURRENT_YJ] == 0)
						floodFillQ(PAST_X + 120, PAST_Y, 'w');
					if (ships[CURRENT_XI + 3][CURRENT_YJ] == 1)
						floodFillQ(PAST_X + 120, PAST_Y, 'g');
					if (ships[CURRENT_XI + 3][CURRENT_YJ] == 2)
						floodFillQ(PAST_X + 120, PAST_Y, 'r');
					if (ships[CURRENT_XI + 3][CURRENT_YJ] == 3)
						floodFillQ(PAST_X + 120, PAST_Y, 'b');
				}
				else {
					if (ships[CURRENT_XI][CURRENT_YJ + 1] == 0)
						floodFillQ(PAST_X, PAST_Y + 40, 'w');
					if (ships[CURRENT_XI][CURRENT_YJ + 1] == 1)
						floodFillQ(PAST_X, PAST_Y + 40, 'g');
					if (ships[CURRENT_XI][CURRENT_YJ + 1] == 2)
						floodFillQ(PAST_X, PAST_Y + 40, 'r');
					if (ships[CURRENT_XI][CURRENT_YJ + 1] == 3)
						floodFillQ(PAST_X, PAST_Y + 40, 'b');
					if (ships[CURRENT_XI][CURRENT_YJ + 2] == 0)
						floodFillQ(PAST_X, PAST_Y + 80, 'w');
					if (ships[CURRENT_XI][CURRENT_YJ + 2] == 1)
						floodFillQ(PAST_X, PAST_Y + 80, 'g');
					if (ships[CURRENT_XI][CURRENT_YJ + 2] == 2)
						floodFillQ(PAST_X, PAST_Y + 80, 'r');
					if (ships[CURRENT_XI][CURRENT_YJ + 2] == 3)
						floodFillQ(PAST_X, PAST_Y + 80, 'b');
					if (ships[CURRENT_XI][CURRENT_YJ + 3] == 0)
						floodFillQ(PAST_X, PAST_Y + 120, 'w');
					if (ships[CURRENT_XI][CURRENT_YJ + 3] == 1)
						floodFillQ(PAST_X, PAST_Y + 120, 'g');
					if (ships[CURRENT_XI][CURRENT_YJ + 3] == 2)
						floodFillQ(PAST_X, PAST_Y + 120, 'r');
					if (ships[CURRENT_XI][CURRENT_YJ + 3] == 3)
						floodFillQ(PAST_X, PAST_Y + 120, 'b');
				}
			}
		}
	}
	else if (step == -1) {
		CURRENT_XI -= 14;
		if ((PAST_X >= 580) && (PAST_Y >= 20) && (PAST_X < 980) && (PAST_Y < 420)) {
			if (ships_enemy[CURRENT_XI][CURRENT_YJ] == 0)
				floodFillQ(PAST_X, PAST_Y, 'w');
			if (ships_enemy[CURRENT_XI][CURRENT_YJ] == 1)
				floodFillQ(PAST_X, PAST_Y, 'w');
			if (ships_enemy[CURRENT_XI][CURRENT_YJ] == 2) 
				floodFillQ(PAST_X, PAST_Y, 'r');
			if (ships_enemy[CURRENT_XI][CURRENT_YJ] == 3) 
				floodFillQ(PAST_X, PAST_Y, 'b');
		}
	}
}

int access(int x, int y) {
	if ((x >= 20) && (y >= 20) && (x < 420) && (y < 420)) {
		
		if (step == 0)
			return 1;
		else if (step == 1) {
			if (VERTICAL == false) {
				if ((x >= 20) && (y >= 20) && (x < 380) && (y < 420))
					return 2;
			}
			else {
				if ((x >= 20) && (y >= 20) && (x < 420) && (y < 380))
					return 3;
			}
		}
		else if (step == 2) {
			if (VERTICAL == false) {
				if ((x >= 20) && (y >= 20) && (x < 340) && (y < 420))
					return 4;
			}
			else {
				if ((x >= 20) && (y >= 20) && (x < 420) && (y < 340))				
					return 5;
			}
		}
		else if (step == 3) {
			if (VERTICAL == false) {
				if ((x >= 20) && (y >= 20) && (x < 300) && (y < 420))
					return 6;
			}
			else {
				if ((x >= 20) && (y >= 20) && (x < 420) && (y < 300))
					return 7;
			}
		}
		return 0;
	}
	else
		return -1;
}

bool accessLocate(int x, int y, int flag) {
	int i, j;
	int left, top, right, bot;
	int x1 = x, y1 = y, x2 = x, y2 = y;
	if (VERTICAL == false)
		x2 = x1 + step;
	else
		y2 = y1 + step;
	if (x == 0)
		left = 0;
	else
		left = x - 1;
	if (y == 0)
		top = 0;
	else
		top = y - 1;
	if (VERTICAL == false) {
		x = x + step;
		if (x  == 9)
			right = 9;
		else
			right = x + 1;
		if (y  == 9)
			bot = 9;
		else
			bot = y + 1;
	}
	else {
		y = y + step;
		if (x  == 9)
			right = 9;
		else
			right = x + 1;
		if (y  == 9)
			bot = 9;
		else
			bot = y + 1;
	}
	if (flag == 1) {
		for (i = left; i <= right; i++)
		{
			for (j = top; j <= bot; j++)
			{
				if (ships[i][j] == 1)
					return false;
			}
		}
		for (i = x1; i <= x2; i++)
			for (j = y1; j <= y2; j++)
				ships[i][j] = 1;
	}
	else {
		for (i = left; i <= right; i++)
		{
			for (j = top; j <= bot; j++)
			{
				if (ships_enemy[i][j] == 1)
					return false;
			}
		}
		for (i = x1; i <= x2; i++)
			for (j = y1; j <= y2; j++) {
				ships_enemy[i][j] = 1;
			}
	}
	return true;
}

void winf(int p = 1) {
	int i, j;
	if (p == 1)
		for (i = 0; i < 10; i++)
			for (j = 0; j < 10; j++) {
				Sleep(10);
				floodFillQ(i * 40 + 580, j * 40 + 20, 'q');
			}
	else
		for (i = 0; i < 10; i++)
			for (j = 0; j < 10; j++){
				Sleep(10);
				floodFillQ(i * 40 + 20, j * 40 + 20, 'q');
			}
	step = -2;
	glutDestroyWindow(1);
}

void mouseMove(int x, int y)
{
	if (step == -2)
		glClearColor(1,1,1,0);
	
	static int PAST_X = 20, PAST_Y = 20;
	x = (x - 20) / 40 * 40 + 20;
	y = (y - 20) / 40 * 40 + 20;
	CURRENT_XI = (x - 20) / 40;
	CURRENT_YJ = (y - 20) / 40;
	while(movepl == false)
	{
		srand(time(0));
		static int win = 0;
		CURRENT_XI = rand() % 10;
		CURRENT_YJ = rand() % 10;
		if (ships[CURRENT_XI][CURRENT_YJ] == 1)
		{
			Sleep(100);
			floodFillQ((CURRENT_XI * 40) + 20, (CURRENT_YJ * 40) + 20, 'r');
			ships[CURRENT_XI][CURRENT_YJ] = 2;
			win++;
			if (win == 20)
				winf(2);
		}
		else if (ships[CURRENT_XI][CURRENT_YJ] == 0)
		{
			Sleep(100);
			ships[CURRENT_XI][CURRENT_YJ] = 3;
			movepl = true;
			floodFillQ((CURRENT_XI * 40) + 20, (CURRENT_YJ * 40) +20, 'b');
		}
	}
	if (step >= 0) {
		if ((x != PAST_X) || (y != PAST_Y)) {
			if (access(PAST_X, PAST_Y))
				watchdog(PAST_X, PAST_Y, VERTICAL);
			if ((x >= 20) && (y >= 20) && (x < 420) && (y < 420)) {
				switch (access(x, y)){
				case 1: { floodFillQ(x, y, 'r'); break;}
				case 2: { floodFillQ(x, y, 'r'); floodFillQ(x + 40, y, 'r'); break;}
				case 3: { floodFillQ(x, y, 'r'); floodFillQ(x, y + 40, 'r'); break;}
				case 4: { floodFillQ(x, y, 'r'); floodFillQ(x + 40, y, 'r'); floodFillQ(x + 80, y, 'r');break;}
				case 5: { floodFillQ(x, y, 'r'); floodFillQ(x, y + 40, 'r'); floodFillQ(x, y + 80, 'r'); break;}
				case 6: { floodFillQ(x, y, 'r'); floodFillQ(x + 40, y, 'r'); floodFillQ(x + 80, y, 'r'); floodFillQ(x + 120, y, 'r'); break;}
				case 7: { floodFillQ(x, y, 'r'); floodFillQ(x, y + 40, 'r'); floodFillQ(x, y + 80, 'r'); floodFillQ(x, y + 120, 'r'); break;}
				default:
					break;
				}
			}
		}
	}
	else if (step == -1) {
		if ((x != PAST_X) || (y != PAST_Y)) {
			watchdog(PAST_X, PAST_Y, VERTICAL);
			if ((x >= 580) && (y >= 20) && (x < 980) && (y < 420)) 
				floodFillQ(x, y, 'r');
		}
	}
	PAST_X = x;
	PAST_Y = y;
}

void drawEnemy()
{
	int i, j;
	for (i = 0; i < 10; i++)
		for (j = 0; j < 10; j++)
		{
			if (ships_enemy[i][j] == 1)
				floodFillQ((i * 40) + 580, (j * 40) + 20, 'g');
		}
}

void mouseButton(int button, int state, int x, int y)
{
	static int count = 0;
	x = (x - 20) / 40 * 40 + 20;
	y = (y - 20) / 40 * 40 + 20;
	int xi = (x - 20) / 40;
	int yj = (y - 20) / 40;
	if (button == GLUT_RIGHT_BUTTON) {
		drawEnemy();
	}
	if (button == GLUT_MIDDLE_BUTTON)
	{
		if (state == GLUT_DOWN) {
			watchdog(x, y, VERTICAL);
			if (VERTICAL == false)
				VERTICAL = true;
			else
				VERTICAL = false;
		}
	}
	else if (button == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN) {
			if (step == -1) {
				static int win = 0;
				xi -= 14;
				if (movepl == true) {
					if ((x >= 580) && (y >= 20) && (x < 980) && (y < 420)) {
						if (ships_enemy[xi][yj] == 1) {
							ships_enemy[xi][yj] = 2;
							floodFillQ(x, y, 'r');
							win++;
							if (win == 20)
								winf();
						}
						else if (ships_enemy[xi][yj] == 0) {
							movepl = false;
							ships_enemy[xi][yj] = 3;
							floodFillQ(x, y, 'b');
						}
					}
				}					
			}
			else if ((step >= 0) && (step <= 3)) {
				if (access(x, y) > 0) {
					if (accessLocate(xi, yj, 1)) {
						static Ship *p;
						watchdog(x, y, VERTICAL);
						switch (count) {
						case 0: {
							step--;
							count++;
							sh4.place_ship(xi, yj, VERTICAL);
							sh4.show();
							break;
						}
						case 1:
							sh3[0].place_ship(xi, yj, VERTICAL);
							count++;
							sh3[0].show();
							break;
						case 2:
							sh3[1].place_ship(xi, yj, VERTICAL);
							count++;
							sh3[1].show();
							step--;
							break;
						case 3:
							sh2[0].place_ship(xi, yj, VERTICAL);
							count++;
							sh2[0].show();
							break;
						case 4:
							sh2[0].place_ship(xi, yj, VERTICAL);
							count++;
							sh2[0].show();
							break;
						case 5:
							sh2[0].place_ship(xi, yj, VERTICAL);
							count++;
							sh2[0].show();
							step--;
							break;
						case 6:
							sh1[0].place_ship(xi, yj, VERTICAL);
							count++;
							sh1[0].show();
							break;
						case 7:
							sh1[0].place_ship(xi, yj, VERTICAL);
							count++;
							sh1[0].show();
							break;
						case 8:
							sh1[0].place_ship(xi, yj, VERTICAL);
							count++;
							sh1[0].show();
							break;
						case 9:
							sh1[0].place_ship(xi, yj, VERTICAL);
							count++;
							sh1[0].show();
							step--;
							break;
						default:
							break;
						}
					}
				}
			}
		}
	}
}

void game()
{
	srand(time(0));
	int i, j;
	for (i = 0; i < 10; i++)
		for (j = 0; j < 10; j++)
		{
			ships[i][j] = 0;
			ships_enemy[i][j] = 0;
		}
	int x, y, temp, count = 0;
	step = 3;
	while (1) {
		x = rand() % 10;
		y = rand() % 10;
		temp = rand() % 10;
		if (temp > 5)
			VERTICAL = false;
		else
			VERTICAL = true;
		if (access((x*40)+20, (y*40)+20) )
			if ( accessLocate(x, y, 2))
			{
				switch (count)
				{
				case 0: step--; break;
				case 2: step--; break;
				case 5: step--; break;
				case 9: step--; break;
				default:
					break;
				}
				count++;
			}
		if (count == 10)
			break;
	}
	step = 3;
}

int main(int argc, char *argv[])
{
	game();
	glutInit(&argc, argv);
	glutInitWindowSize(1000, 600);
	glutInitWindowPosition(100, 100);
	glutInitDisplayMode(GLUT_RGB);
	glutCreateWindow("SeaWar");
	glutMouseFunc(mouseButton);
	glutPassiveMotionFunc(mouseMove);
	glutReshapeFunc(Reshape);
	glutDisplayFunc(Draw);
	glClearColor(1, 1, 1, 0);
	glutMainLoop();
	return 0;
}