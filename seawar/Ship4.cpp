#include "Ship4.h"
#include <iostream>
using namespace std;
void Ship4::place_ship(int x, int y, bool vert)
{
	this->x = x;
	this->y = y;
	vertical = vert;
}

void  Ship4::show() {
	cout << "Ship4 was located on x: " << x << " and y: " << y << endl;
};

Ship4::Ship4()
{
	place_ship(99, 99, false);
	deck = 4;
}

Ship4::Ship4(int x, int y, bool vert)
{
	place_ship(x, y, vert);
	deck = 4;
}


Ship4::~Ship4()
{
}
